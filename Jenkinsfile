pipeline{
    agent {
        label "vm1"
    }
    options {
        parallelsAlwaysFailFast()
        buildDiscarder(logRotator(numToKeepStr: '1'))
        // skipDefaultCheckout()
    }

    triggers {
        GenericTrigger (
            causeString: 'push to $ref', 
            genericVariables: [
                [key: 'object_kind', value: '$.object_kind'], 
                [key: 'total_commits_count', value: '$.total_commits_count'],
                [key: 'after', value: '$.after'], 
                [key: 'ref', value: '$.ref'], 
                [key: 'git_ssh_url', value: '$.repository.git_ssh_url']
            ], 
            printContributedVariables: true, 
            printPostContent: true, 
            regexpFilterExpression: '^push\\s.{40}\\s[^0].*', 
            regexpFilterText: '$object_kind $after $total_commits_count', 
            token: 'bb154841-8750-4eb5', 
            tokenCredentialId: ''
        )
    }

    stages{
        // 1. 环境准备
        stage("Prepare-Env"){
            when {
                beforeAgent true
                not {changelog '^%%%:.*$'} 
            }

            steps {
                script {
                    // env.ENV = sh(script:"if [ ${BRANCH_NAME} = 'main' -o ${BRANCH_NAME} = 'master' ]; then echo 'prod'; else echo 'test'; fi", returnStdout:true)
                    if (env.BRANCH_NAME == 'main' || env.BRANCH_NAME == 'master') {
                        env.ENV = 'prod'
                    } else {
                        env.ENV = 'test'
                    }

                    env.IMAGE_REGISTRY = "registry.cn-chengdu.aliyuncs.com/yangsj"

                    env.COMMIT_NAME=sh(script: "git log -n 1 --pretty=format:%cn", returnStdout: true)
                    env.COMMIT_MSG=sh(script: "git log -n 1 --pretty=format:%s", returnStdout: true)
                    env.COMMIT_TIME=sh(script: "git log -n 1 --pretty=format:%ci", returnStdout: true)
                }
            }
        }

        // 2.打包编译: 构建镜像、推送镜像、部署服务
        stage("Build-App"){
            when {
                beforeAgent true
                not {changelog '^%%%:.*$'} 
            }

            matrix {
                axes {
                    axis {
                        name "SERVICE"
                        values "access", "users"
                    }
                }
                stages {
                    stage("build-prepare"){
                        when {
                            anyOf {
                                changelog '.*--force-ci$'
                                changeset "services/${SERVICE}/*"
                            }
                        }
                        environment {
                            DOCKERFILE_PATH="services/${SERVICE}/Dockerfile"
                            DOCKER_IMAGE="${IMAGE_REGISTRY}/${SERVICE}:${ENV}"
                            DOCKER_COMPOSE="docker-compose/${SERVICE}-docker-compose.yaml"
                        }
                        stages {
                            stage("docker-build"){
                                steps{
                                    sh "docker build . -f ${DOCKERFILE_PATH} -t ${DOCKER_IMAGE} --network=host"
                                }
                            }
                            stage("docker-push"){
                                steps {
                                    sh "docker image prune -f"
                                    sh "docker push ${DOCKER_IMAGE}"
                                }   
                            }
                            stage("deploy-app"){
                                steps {
                                    sh "docker-compose -f ${DOCKER_COMPOSE} up -d ${ENV}"
                                }
                            }
                        }
                    }
                }
            }
        }

        // 3.部署服务
        stage("Notify-Wechat"){
            steps{
              echo "node_name: ${NODE_NAME}"
              echo "project/branch: ${JOB_NAME}"
              echo "commit_id: ${GIT_COMMIT}"
              echo "commit_name: ${COMMIT_NAME}"
              echo "commit_msg: ${COMMIT_MSG}"
              echo "commit_time: ${COMMIT_TIME}"
              echo "build_status: ${currentBuild.currentResult}"
              echo "${sh(script:"printenv", returnStdout:true)}"
            }
        }
    }
}
