package main

import (
	"fmt"
	"net"
	"net/http"
)

func main() {
	listener, _ := net.Listen("tcp", "0.0.0.0:8009")

	mux := http.NewServeMux()
	mux.HandleFunc("/access", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("access"))
	})

	http.Serve(listener, mux)
	fmt.Println("access server is running on port 8009 ")
}
