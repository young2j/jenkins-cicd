package utils

import "net"

// 根据域名获取ip
func GetIpsByDomain(domain string) (ipStrs []string) {
	ips, _ := net.LookupIP(domain)
	for _, ip := range ips {
		ipStrs = append(ipStrs, ip.String())
	}
	return
}
